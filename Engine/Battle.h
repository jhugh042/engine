#ifndef BATTLE_H
#define BATTLE_H

#include <SDL_mixer.h>
#include "Entity.h"


class Battle
{
public:
	Battle();
	Battle(const char* musicFile, const char* startMusicFile);
	~Battle();

	void battleStartAnimation(Graphics &graphics, Map &map, Entity &player, SDL_Rect &camera);


	bool startBattleLoop();
	bool playMusic();
	bool playStartEffect();
	bool pauseBGMusic();
	void haltBGMusic();

protected:
private:
	Mix_Music * battleMusic;
	Mix_Chunk * battleStart;
};

#endif