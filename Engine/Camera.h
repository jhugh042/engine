#pragma once

#include "Location.h"
#include "Globals.h"
#include <iostream>

class Camera
{
public:
	Camera();
	Camera(Location playerLoc);
	~Camera();
	void update(Location playerLoc);


	Location getOffsetInverse();
private:
	Location offset;

	int maxX = (globals::SCREEN_WIDTH / 2) + 20; //340
	int maxY = (globals::SCREEN_HEIGHT / 2) + 20; //260
	int minX = (globals::SCREEN_WIDTH / 2) - 20; //300
	int minY = (globals::SCREEN_HEIGHT / 2) - 20;//220
};

