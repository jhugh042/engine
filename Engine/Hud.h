#ifndef HUD_H
#define HUD_H

#include "Sprite.h"

class Graphics;

class Hud
{
public:
	Hud();
	Hud(Graphics &graphics, const std::string &filePath,
		Location sourceLoc, int width, int height,
		Location posLoc,bool _visible);
	~Hud();

	/**
	@brief draws the hud to screen
	@param	Graphics&
	*/
	void draw(Graphics &graphics);

	/**
	@brief sets the visibility of Hud item
	@param	bool visible?
	*/
	void setVisibility(bool _visibility);

	/**
	@brief sets the visibility to opposite of what it currently is
	*/
	void changeVisibility();

protected:
private:
	Sprite hudItem;
	bool visible = false;
	Location location;
};

#endif