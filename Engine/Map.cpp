#include "Map.h"



Map::Map(){

}


Map::~Map(){
	//delete backgroundMusic;
	//delete localTiles;
	tiles.clear();
	//gameMap.clear();
	map.clear();
}

bool Map::draw(Graphics &graphics, SDL_Rect camera) {
	int lx = (camera.x / globals::SCREEN_WIDTH);
	lx = (lx - 1) * globals::SCREEN_WIDTH;
	int ly = (camera.y / globals::SCREEN_HEIGHT);
	ly = (ly - 1)* globals::SCREEN_HEIGHT;
	int ux = lx + (globals::SCREEN_WIDTH * 3);
	int uy = ly + (globals::SCREEN_HEIGHT * 3);

	std::pair<int, int> lowerKey = std::make_pair(lx, ly);
	std::pair<int, int> upperKey = std::make_pair(ux, uy);

	std::multimap<std::pair<int, int>, std::pair<std::pair<int, int>, MapTiles*>>::iterator 
			it1 = map.lower_bound(lowerKey);
	std::multimap<std::pair<int, int>, std::pair<std::pair<int, int>, MapTiles*>>::iterator
		it2 = map.lower_bound(upperKey);

	int playerX = (camera.x + (globals::SCREEN_WIDTH / 2));
	int playerY = (camera.y + (globals::SCREEN_HEIGHT / 2));
	while (it1 != it2) {
		int x = it1->second.first.first;
		int y = it1->second.first.second;
		it1->second.second->sprite.draw(
			graphics,
			Location(x - camera.x, y - camera.y),
			false
		);
		if (x >= playerX && x <= playerX + globals::TILE_SIZE) {
			if (y >= playerY && y <= playerY + globals::TILE_SIZE) {
				playerPos = &(it1->second);
			}
		}
		++it1;
	}
	return playerPos->second->walkable;
}

void Map::loadMap(std::map <std::pair<int,int>, std::string> givenMap) {
	//iterates given map
	for (std::map<std::pair<int, int>, std::string>::iterator mIT = givenMap.begin(); mIT != givenMap.end(); ++mIT) {
		for (std::vector<MapTiles>::iterator vIT = tiles.begin(); vIT != tiles.end(); ++vIT) {
			//if names match
			if (mIT->second == vIT->name) {
				MapTiles* insert = &(*vIT);
				int x = mIT->first.first;
				int y = mIT->first.second;
				int xi = x / globals::SCREEN_WIDTH;
				int yj = y / globals::SCREEN_HEIGHT;

				std::pair<std::pair<int, int>, MapTiles*> mapTiles;
				mapTiles = std::make_pair(
						std::make_pair(x,y), 
						insert);
				map.insert(
					std::make_pair(std::make_pair(xi * globals::SCREEN_WIDTH, yj*globals::SCREEN_HEIGHT),
						mapTiles));
			}
		}
	}
}

void Map::addTile(Sprite sprite, std::string name, bool walkable) {
	MapTiles tile;
	tile.sprite = sprite;
	tile.name = name;
	tile.walkable = walkable;
	tiles.push_back(tile);
}

bool Map::setBGMusic(const char* filename) {
	backgroundMusic = Mix_LoadMUS(filename);
	if (backgroundMusic == NULL) {
		return false;
	}
	return true;
}

bool Map::playBGMusic() {
	if (Mix_PlayingMusic() == 0) {
		if (Mix_PlayMusic(backgroundMusic, -1) == -1) {
			return false;
		}
	}
	if (Mix_PausedMusic()) {
		Mix_ResumeMusic();
	}
	return true;
}

bool Map::pauseBGMusic() {
	if (Mix_PlayingMusic() != 0) {
		 Mix_PauseMusic();
	}

	if (Mix_PausedMusic()) {
		return true;
	}
	return false;
}

void Map::haltBGMusic() {
	Mix_HaltMusic();
}

bool Map::checkWalkable(float _x,float _y) {


	int minx = (_x/globals::SCREEN_WIDTH);
	minx = minx * globals::SCREEN_WIDTH;

	int miny = (_y / globals::SCREEN_HEIGHT);
	miny = miny * globals::SCREEN_HEIGHT;

	std::pair<int, int> min = std::make_pair(minx, miny);

	int posx = (_x / globals::TILE_SIZE);
	posx = posx * globals::TILE_SIZE;

	int posy = (_y / globals::TILE_SIZE);
	posy = posy * globals::TILE_SIZE;

	std::pair<int, int> pos = std::make_pair(posx, posy);

	std::multimap<std::pair<int, int>, std::pair<std::pair<int, int>, MapTiles*>>::iterator
		it1 = map.find(min);
	if (it1 != map.end()) {
		if (it1->first == pos) {
			//if(it1->)
			std::cout << "on Non-walkable\n";
		}
	}
	/*
	std::multimap<std::pair<int, int>, std::pair<std::pair<int, int>, MapTiles*>>::iterator
		it2 = map.lower_bound(upperKey);
	while (it1 != it2) {
		int x = it1->second.first.first;
		int y = it1->second.first.second;
		if (_x >= x && _x <= x + 36) {
			if (_y >= y && _y <= y + 36) {
				return it1->second.second->walkable;
			}
		}
		++it1;
	}
	return false;
	*/
	return false;
}