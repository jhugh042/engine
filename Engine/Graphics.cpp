#include "Graphics.h"



Graphics::Graphics(){

}

Graphics::~Graphics(){
	SDL_DestroyWindow(window);
}

void Graphics::initialize() {
	SDL_CreateWindowAndRenderer(globals::SCREEN_WIDTH, globals::SCREEN_HEIGHT, 0, &window, &renderer);
	SDL_SetWindowTitle(window, "Game");
}

SDL_Surface* Graphics::loadImage(const std::string &filePath) {
	if (spriteSheets.count(filePath) == 0) {
		spriteSheets[filePath] = IMG_Load(filePath.c_str());
	}
	return spriteSheets[filePath];
}

void Graphics::blitSurface(SDL_Texture *source, SDL_Rect* sourceRectangle, SDL_Rect* destRectangle, bool facingLeft) {
	if (facingLeft) {
		SDL_RenderCopyEx(renderer, source, sourceRectangle, destRectangle, 0, 0, SDL_FLIP_HORIZONTAL);
	}
	else {
		SDL_RenderCopyEx(renderer, source, sourceRectangle, destRectangle, 0, 0, SDL_FLIP_NONE);
	}
}
void Graphics::drawColoredBox(SDL_Rect* rect) {
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderDrawRect(renderer, rect);
	SDL_RenderFillRect(renderer, rect);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

void Graphics::flip() {
	SDL_RenderPresent(renderer);
}

void Graphics::clear() {
	SDL_RenderClear(renderer);
}

SDL_Renderer* Graphics::getRenderer() const {
	return renderer;
}
