#include "Hud.h"



Hud::Hud(){

}

Hud::Hud(Graphics &graphics, const std::string &filePath,
		Location sourceLoc, int width, int height,
		Location posLoc, bool _visible) {
	hudItem = Sprite(graphics, filePath, sourceLoc, width, height, posLoc);
	visible = _visible;
	location = posLoc;
}

Hud::~Hud(){

}
void Hud::setVisibility(bool _visibility) {
	visible = _visibility;
}

void Hud::changeVisibility() {
	if (visible) {
		visible = false;
	}
	else {
		visible = true;
	}
}

void Hud::draw(Graphics &graphics) {
	if (visible) {
		hudItem.draw(graphics, location, false);
	}
}