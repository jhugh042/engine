#ifndef GAME_H
#define GAME_H

#include "Input.h"
#include "Map.h"
#include "Battle.h"
#include <stdlib.h>
#include <time.h>

//TODO : https://www.youtube.com/watch?v=QeN1ygJD5y4
class Game
{
public:
	Game();
	~Game();

	/**
	@brief sets the graphics of the Game Object
	@param &Graphics
	*/
	void setGraphics(Graphics &graphics);

	/**
	@brief sets the player of the Game Object
	@param &Entity
	*/
	void setPlayer(Entity &_player);

	/**
	@brief sets the Map of the Game Object
	@param &Map
	*/
	void setMap(Map &_map);

	/**
	@brief starts the game loop
	*/
	void start();

	bool randomEnemyEncounter(float value);
private:

	/**
	@brief draws all the necessary objects
	*/
	void draw();

	/**
	@brief updates the game with any changes
	@param elapsed time
	*/
	void update(int _ELAPSED_TIME);

	const char* battleStart = "C:\\Users\\jmhug\\source\\repos\\Engine\\Resources\\Battle-start.wav";
	const char* battleMusic = "C:\\Users\\jmhug\\source\\repos\\Engine\\Resources\\background-battle.mp3";

	SDL_Rect camera = {0,0,globals::SCREEN_WIDTH,globals::SCREEN_HEIGHT};
	Graphics graphics;
	Input input;
	SDL_Event event;
	Entity player;
	Map map;
	Location previous;
};

#endif