#pragma once

#include "Graphics.h"
#include "Location.h"
#include "Tile.h"
#include <vector>
#include <SDL_mixer.h>

//maybe look at useing a struct for tiles instead?
struct MapTiles {
	Sprite sprite;
	std::string name;
	bool walkable = true;
	bool operator < (const MapTiles& rhs) const 
	{
		return this->name < rhs.name;
	}
};

//order:
/*
Add tiles
then load map
then draw
*/
class Map{
public:
	Map();
	~Map();

	/**
	@brief loads the map, currently used for testing
	@param	map<<int,int> string>
	*/
	void loadMap(std::map <std::pair<int, int>, std::string> map);

	/**
	@brief draws the map tiles to screen
	@param	Graphics&, Camera
	@todo only draw local tiles
	*/
	bool draw(Graphics &graphics, SDL_Rect camera);


	/**
	@brief adds tile to map
	@param Tile
	@todo
	*/
	void addTile(Sprite sprite, std::string name, bool walkable = true);

	/**
	@brief sets the background music of the map
	@param file of music
	*/
	bool setBGMusic(const char* filename);

	/**
	@brief plays the bakground music
	*/
	bool playBGMusic();

	bool pauseBGMusic();

	bool checkWalkable(float _x, float _y);

	void haltBGMusic();
private:
	Mix_Music * backgroundMusic;
	std::vector<MapTiles> tiles;
	//maybe try having a map of game map where each entry contains 0 - camera.w size
	//std::map <std::pair<int, int>, MapTiles*> gameMap;
	//std::map <std::pair<int, int>, MapTiles*> localMap;
			//((minx,miny),ptr to map)
	std::multimap <
		std::pair<int, int>,
		std::pair<std::pair<int, int>, MapTiles*>> map;

	std::pair<std::pair<int, int>, MapTiles*>* playerPos;
	float minX = 0;
	float minY = 0;
	float maxX = 0;
	float maxY = 0;
};

