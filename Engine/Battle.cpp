#include "Battle.h"



Battle::Battle(){

}

Battle::Battle(const char* musicFile, const char* startMusicFile) {
	battleStart = Mix_LoadWAV(startMusicFile);
	battleMusic = Mix_LoadMUS(musicFile);
	if (battleMusic == NULL) {
		printf("Error loading battle Music\n");
	}
}

Battle::~Battle(){
	//delete battleMusic;
}

void Battle::battleStartAnimation(Graphics &graphics, Map &map, Entity &player,SDL_Rect &camera) {
	//stop bg music

	if (!map.pauseBGMusic()) {
		printf("Failed to pause music");
	}
	map.haltBGMusic();
	playStartEffect();

	//clear map one column at a time
	int x = 0;
	while (x <= globals::SCREEN_WIDTH) {
		graphics.clear();
		map.draw(graphics, camera);
		for (int i = 0; i <= globals::SCREEN_HEIGHT; i++) {
			SDL_Rect rect = { 0,i,x + (rand() % 50 + 1),1 };
			graphics.drawColoredBox(&rect);
		}
		player.draw(graphics, camera);


		graphics.flip();
		x = x + 5;
	}
	//after map is clear, clear player
	graphics.clear();
	graphics.flip();
}

bool Battle::startBattleLoop() {
	if (!playMusic()) {
		printf("Error playing battle music.");
	}

	printf("Press ESC to exit battle");
	SDL_Event event;
	while (true) {

		if (SDL_PollEvent(&event)) {
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				break;
			}
		}

	}
	haltBGMusic();
	return true;
}

bool Battle::playMusic() {
	if (Mix_PlayingMusic() == 0) {
		if (Mix_PlayMusic(battleMusic, -1) == -1) {
			return false;
		}
		if (Mix_PlayingMusic() == 1) {
			return true;
		}
	}
	printf("Music already playing?");
	return false;
}

bool Battle::playStartEffect() {
	//-1 finds first available channel to play in, 0 says to play once
	if (Mix_PlayChannel(-1, battleStart, 0) == -1) {
		return true;
	}
	return false;
}

void Battle::haltBGMusic() {
	Mix_HaltMusic();
}

bool Battle::pauseBGMusic() {
	if (Mix_PlayingMusic() != 0) {
		Mix_PauseMusic();
	}

	if (Mix_PausedMusic()) {
		return true;
	}
	return false;
}