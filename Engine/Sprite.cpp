#include "Sprite.h"


Sprite::Sprite() {

}

Sprite::Sprite(Graphics &graphics, const std::string &filePath,
	Location sourceLoc, int width, int height,
	Location posLoc){

	loc = posLoc;
	sourceRect.x = sourceLoc.getX();
	sourceRect.y = sourceLoc.getY();
	sourceRect.w = width;
	sourceRect.h = height;
	spriteSheet = SDL_CreateTextureFromSurface(graphics.getRenderer(),
											   graphics.loadImage(filePath));

	if (spriteSheet == NULL) {
		printf("Error loading image: %s\n" , filePath);
	}
}

Sprite::~Sprite() {

}

/*
	Draws sprite to screen
*/
void Sprite::draw(Graphics &graphics, Location _loc, bool facingLeft) {
	SDL_Rect destinationRect = { _loc.getX(), _loc.getY(),
		(sourceRect.w * destinationScale()), (sourceRect.h * destinationScale()) };
	graphics.blitSurface(spriteSheet, &sourceRect, &destinationRect, facingLeft);
}

/*
	updates animation frames
*/
void Sprite::update(int elapsedTime) {

}

float Sprite::destinationScale() {
	float wantedWidth = globals::TILE_SIZE;
	return wantedWidth/sourceRect.w;
}
/*
float Sprite::destinationHeight(float h) {
	return sourceRect.h * h;
}
*/