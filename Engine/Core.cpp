#ifndef CORE
#define CORE

#include "Game.h"

Player loadPlayerAnimation(std::string spriteLoc, Graphics &graphics);
Player loadPlayerAnimation2(std::string spriteLoc, Graphics &graphics);

int main(int argc, char *argv[]) {

	Game game = Game();
	Graphics graphics;
	graphics.initialize();

	//std::string spriteLoc = "C:\\Users\\jmhug\\source\\repos\\Engine\\Resources\\character2.gif";
	std::string spriteLoc2 = "C:\\Users\\jmhug\\source\\repos\\Engine\\Resources\\Walk.png";
	//Player player = loadPlayerAnimation(spriteLoc, graphics);

	Player player = loadPlayerAnimation2(spriteLoc2, graphics);

	Map map;

	Sprite testSprite = Sprite(
		graphics,
		"C:\\Users\\jmhug\\source\\repos\\Engine\\Resources\\Grass.png",
		Location(0, 0),
		160, 160,
		Location(0, 0)
	);
	Sprite testSprite2 = Sprite(
		graphics,
		"C:\\Users\\jmhug\\source\\repos\\Engine\\Resources\\Wall_Base.gif",
		Location(0, 0),
		160, 160,
		Location(0, 0)
	);
	map.addTile(testSprite, "TEST");
	map.addTile(testSprite2, "WALL",false);
	std::map<std::pair<int,int>, std::string> testMap;
	for (int i = -100; i <= 100; i++) {
		for (int j = -100; j <= 100; j++) {
			testMap.insert(
				std::pair<std::pair<int, int>, std::string>
					(std::make_pair(i*36,j*36), "TEST"));
		}
	}
	for (int i = -100; i <= 100; i++) {
		std::pair<int, int> val = std::make_pair(i * 36, 36);
		auto it = testMap.find(val);
		if (it != testMap.end()) {
			it->second = "WALL";
		}
		//testMap.
		/*testMap.insert(
			std::pair<std::pair<int, int>, std::string>
			(std::make_pair(i * 36, 36), "WALL"));*/
	}
	map.loadMap(testMap);
	std::cout << "Done loading map";
	//TODO load map details and map BG music
	game.setMap(map);
	game.setGraphics(graphics);
	game.setPlayer(player);

	game.start();

	return 0;
}


Player loadPlayerAnimation(std::string spriteLoc, Graphics &graphics) {

	int _numanimation = 8;
	int _aFrames[] = { 9,9,9,9,9,9,9,9 };

	//
	Location _aStartLoc[] = { Location(10,11),Location(10,101),Location(10,193),Location(10,287),
							  Location(10,379),Location(10,471),Location(10,563),Location(10,655) };

	std::pair<SDL_Scancode,SDL_Scancode> _aName[] = { 
		std::make_pair(SDL_SCANCODE_DOWN,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_DOWN,SDL_SCANCODE_LEFT),
		std::make_pair(SDL_SCANCODE_LEFT,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_UP,SDL_SCANCODE_LEFT),
		std::make_pair(SDL_SCANCODE_UP,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_UP,SDL_SCANCODE_RIGHT),
		std::make_pair(SDL_SCANCODE_RIGHT,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_DOWN,SDL_SCANCODE_RIGHT)
	};

	int _aWidth[] = { 30,30,30,30,30,30,30,30 };
	int _aHeight[] = { 75,75,75,75,75,75,75,75 };
	Location _aoffset[] = { Location(17,0),Location(17,0),Location(17,0),Location(17,0),
							Location(17,0),Location(17,0),Location(17,0),Location(17,0) };

	AnimatedSprite sprite = AnimatedSprite(graphics, spriteLoc, Location(10, 10), 30, 75, Location(globals::SCREEN_WIDTH / 2, globals::SCREEN_HEIGHT/2),
		_aFrames, _aStartLoc, _aName, _aWidth, _aHeight, _aoffset, _numanimation);

	Player player = Player(1, "player", sprite, Location(globals::SCREEN_WIDTH / 2, globals::SCREEN_HEIGHT/2));
	return player;
}

Player loadPlayerAnimation2(std::string spriteLoc, Graphics &graphics) {

	int _numanimation = 8;
	int _aFrames[] = { 8,8,8,8,8,8,8,8 };

	//
	Location _aStartLoc[] = { 
		Location(100,25),
		Location(100,325),
		Location(100,625), 
		Location(100,925), 
		Location(100,1225), 
		Location(100,1525), 
		Location(100,1825), 
		Location(100,2125) };

	std::pair<SDL_Scancode, SDL_Scancode> _aName[] = {
		std::make_pair(SDL_SCANCODE_DOWN,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_DOWN,SDL_SCANCODE_RIGHT),
		std::make_pair(SDL_SCANCODE_RIGHT,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_UP,SDL_SCANCODE_RIGHT),
		std::make_pair(SDL_SCANCODE_UP,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_UP,SDL_SCANCODE_LEFT),
		std::make_pair(SDL_SCANCODE_LEFT,SDL_SCANCODE_UNKNOWN),
		std::make_pair(SDL_SCANCODE_DOWN,SDL_SCANCODE_LEFT)
	};

	int _aWidth[] = { 100,100,100,100,100,100,100,100 };
	int _aHeight[] = {250,250,250,250,250,250,250,250 };
	Location _aoffset[] = { Location(200,0),Location(200,0) ,Location(200,0),
		Location(200,0) ,Location(200,0) ,Location(200,0) ,Location(200,0), Location(200,0)};

	AnimatedSprite sprite = AnimatedSprite(graphics, spriteLoc, Location(100, 25),100,250, Location(globals::SCREEN_WIDTH / 2, globals::SCREEN_HEIGHT / 2),
		_aFrames, _aStartLoc, _aName, _aWidth, _aHeight, _aoffset, _numanimation);

	Player player = Player(1, "player", sprite, Location(globals::SCREEN_WIDTH / 2, globals::SCREEN_HEIGHT / 2));
	return player;
}
#endif