#pragma once

#include <SDL.h>
#include <map>
#include "Player.h"
#include "Map.h"

class Input
{
public:

	/**
		@brief Clears the maps of pressed and released keys
	*/
	void update();

	/**
		@brief registers key released
	*/
	void keyUp(const SDL_Event &event);

	/**
		@brief registers key pressed
	*/
	void keyDown(const SDL_Event &event);

	/**
		@param key value
		@return returns true if key is pressed
	*/
	bool keyPressed(SDL_Scancode key);

	/**
	@param key value
	@return returns true if key was released
	*/
	bool keyReleased(SDL_Scancode key);

	/**
	@param key value
	@return returns true if key is held
	*/
	bool keyHeld(SDL_Scancode key);

	/**
		@brief	handles how the input/key pressed affects the player		
		@param Entity object to be affected
		@return true, false if ESC is pressed
	*/
	virtual bool keyHandler(Entity &player);

	/**
	TODO
	*/
	void keyMap(SDL_Scancode keypressed);

	void clearAllKeys();

private:
	std::map<SDL_Scancode, bool> heldKeys;
	std::map<SDL_Scancode, bool> pressedKeys;
	std::map<SDL_Scancode, bool> releasedKeys;

	SDL_Scancode MOVE_LEFT = SDL_SCANCODE_LEFT;
	SDL_Scancode MOVE_RIGHT = SDL_SCANCODE_RIGHT;
	SDL_Scancode MOVE_UP = SDL_SCANCODE_UP;
	SDL_Scancode MOVE_DOWN = SDL_SCANCODE_DOWN;
	SDL_Scancode ESCAPE = SDL_SCANCODE_ESCAPE;
	SDL_Scancode UNKNOWN = SDL_SCANCODE_UNKNOWN;
	SDL_Scancode HUD_1 = SDL_SCANCODE_I;
};

