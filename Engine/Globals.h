#ifndef GLOBAL_H
#define GLOBAL_H


namespace globals {
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
	const float SPRITE_SCALE = 2.0f;

	const float BACKGROUND_VOLUME = 0.1;//0 to 1
	const float SOUND_EFFECT_VOLUME = 0.1;
	const float TILE_SIZE = 36.0;
}
#endif
