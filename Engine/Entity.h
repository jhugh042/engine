#ifndef ENTITY_H
#define ENTITY_H

#include <iostream>
#include <SDL_mixer.h>
#include "AnimatedSprite.h"
#include "Location.h"
#include "Hud.h"
#include "Map.h"

class Entity
{
public:
	/**
	@brief TODO
	*/
	Entity();

	/**
	@brief	default constructor for Entity
	@param	sets health value, name, the animations, and location
	*/
	Entity(int _health, std::string name, AnimatedSprite _sprite, Location _loc);

	/**
	@brief TODO
	*/
	~Entity();

	/**
	@brief draws the entity to the screen
	@param Graphics, Camera
	*/
	void draw(Graphics &graphics, SDL_Rect camera);
	virtual void update();

	/**
	@brief gets a reference to tha Animated sprite
	@return AnimatedSprite
	*/
	AnimatedSprite& getSprite();

	/**
	@brief Moves the entity and plays the needed animation
	@param  (-1, 0, 1), (-1, 0, 1), button1, button2
	*/
	virtual void moveEntity(int _x, int _y, SDL_Scancode value1, SDL_Scancode value2);

	/**
	@brief TODO: Should be removed 
	*/
	void move(float _x, float _y);

	/**
	@brief TODO: Should be removed
	*/
	void move(bool left);

	/**
	@brief Changes the selected HUD's visibility
	@param the button linked to the HUD
	@return true for success false otherwise
	*/
	bool changeHUDVisiblity(SDL_Scancode value);

	/**
	@brief gets the current location of the entity
	@return Location
	*/
	Location getLocation() { return loc; }

	/**
	@brief currently stops any playing animation
	*/
	virtual void stopAnimation() { sprite.stopAnimation(); }

	/**
	@brief Adds a sound effect to map of possible sound effects
	@param name Key value, filename of sound effect
	@return true for success
	*/
	bool addSoundEffect(std::string name, const char* filename);

	/**
	@brief plays select sound effect
	@param name of Key value
	@return true for playing
	*/
	bool playSoundEffect(std::string name);

	void revertMove();

protected:
	void takeDamage(float damage);
	void healDamage(float healAmmount);

	std::map<std::string, Mix_Chunk*> soundEffects;

	Location loc;
	int MAX_health;
	float health;
	std::string name;
	AnimatedSprite sprite;
	bool lookingLeft = false;
	std::pair<float, float> lastMove;

private:
	std::map<SDL_Scancode, Hud> entityHud;

	float WALK_SPEED = globals::SCREEN_HEIGHT/100;
};

#endif