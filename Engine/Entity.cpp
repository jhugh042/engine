#include "Entity.h"

Entity::Entity(){

}

Entity::Entity(int _health, std::string _name, AnimatedSprite _sprite, Location _loc) {
	health = _health;
	MAX_health = _health;
	name = _name;
	sprite = _sprite;
	loc = _loc;
}

Entity::~Entity(){

}

AnimatedSprite& Entity::getSprite() {
	return sprite;
}


void Entity::draw(Graphics &graphics,SDL_Rect camera) {
	sprite.draw(graphics, Location(loc.getX() - camera.x, loc.getY() - camera.y), lookingLeft);
	for (std::map<SDL_Scancode, Hud>::iterator it = entityHud.begin(); it != entityHud.end(); ++it) {
		it->second.draw(graphics);
	}

}


void Entity::update() {
	printf("\n Update not implemented for Entity::%s", name);
}


void Entity::takeDamage(float damage) {
	health = health - damage;
	if (health <=0) {
		health = 0;
	}
}

void Entity::healDamage(float healAmmount) {
	health = health + healAmmount;
	if (health == MAX_health) {
		health = MAX_health;
	}
}


void Entity::moveEntity(int _x, int _y, SDL_Scancode value1, SDL_Scancode value2) {
	
	std::pair<SDL_Scancode,SDL_Scancode> code = std::make_pair(value1,value2);
	/*if (value1 == SDL_SCANCODE_LEFT ||
		value1 == SDL_SCANCODE_RIGHT ||
		value1 == SDL_SCANCODE_UP ||
		value1 == SDL_SCANCODE_DOWN ) {
		code = SDL_SCANCODE_LEFT;
	}*/
	//x=1 is right
	//x=-1 is left
	//y=-1 up
	//y=1 down
	//if (map.checkWalkable((loc.getX() + (_x * WALK_SPEED)), (loc.getY() + (_y * WALK_SPEED)))) {
		switch (_x) {
		case 1://moving right
			switch (_y) {
			case 1: //moving NE
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			case 0: //not moving on y axis
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			case -1: //moving SE
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			}
			break;
		case 0: //not moving on x axis
			switch (_y) {
			case -1: //moving up
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			case 1: //moving down
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			}
			break;
		case -1: //moving left
			switch (_y) {
			case 1: //moving NW
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			case 0: //not moving on y axis
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			case -1: //moving SW
				move((_x * (WALK_SPEED)), (_y * (WALK_SPEED)));
				sprite.playAnimation(code, false);
				break;
			}
			break;
		}
	//}
}


void Entity::move(bool left) {
	if (left) {
		lookingLeft = true;
	}
	else{
		lookingLeft = false;
	}
}

void Entity::move(float _x, float _y) {
	/*if (_x > 0) {
		lookingLeft = false;
	}
	else {
		lookingLeft = true;
	}*/
	loc.setY(loc.getY() + _y);
	loc.setX(loc.getX() + _x);
	lastMove = std::make_pair(_x, _y);
}

void Entity::revertMove() {
	loc.setY(loc.getY() - lastMove.second);
	loc.setX(loc.getX() - lastMove.first);
}

bool Entity::changeHUDVisiblity(SDL_Scancode value) {
	std::map<SDL_Scancode, Hud>::iterator pos = entityHud.find(value);
	if (pos == entityHud.end()) {
		return false;
	}
	else {
		pos->second.changeVisibility();
	}
}

bool Entity::addSoundEffect(std::string name, const char* filename) {
	Mix_Chunk *sound = Mix_LoadWAV(filename);
	if (sound == NULL) {
		return false;
	}
	else {
		soundEffects.insert(std::pair<std::string, Mix_Chunk*>(name, sound));
	}
}
bool Entity::playSoundEffect(std::string name) {
	std::map<std::string, Mix_Chunk*>::iterator pos = soundEffects.find(name);
	if (pos == soundEffects.end()) {
		return false;
	}
	else {
		//-1 finds first available channel to play in, 0 says to play once
		if (Mix_PlayChannel(-1, pos->second, 0) == -1) {
			return true;
		}
	}
}