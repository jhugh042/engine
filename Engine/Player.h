#ifndef PLAYER_H
#define PLAYER_H

#include "Hud.h"
#include "Entity.h"


class Player : public Entity
{
public:
	Player();
	Player(int _health, std::string _name, AnimatedSprite _sprite, Location loc);
	Player(const Player &obj);
	~Player();

	void update(Location offset);
private:
	//Hud hud;
	float WALK_SPEED = 0.2;
};

#endif