#pragma once

#include "Sprite.h"
#include "Location.h"
#include "Globals.h"

#include <SDL.h>

class Tile
{
public:
	Tile();
	Tile(Sprite _sprite);
	Tile(Graphics &graphics);
	~Tile();

	void draw(Graphics &graphics, SDL_Rect camera, Location loc);

private:
	Sprite sprite;
};

