#ifndef SPRITE_H
#define SPRITE_H

#include "Graphics.h"
#include <string>
#include "Location.h"
#include "Globals.h"

class Sprite {
public:
	Sprite();
	Sprite(Graphics &graphics, const std::string &filePath,
		Location sourceLoc, int width, int height,
		Location posLoc);
	~Sprite();

	/**
	@brief draws the sprite to the screen
	@param Graphics, Location, Bool to flip
	*/
	void draw(Graphics &graphics, Location _loc, bool facingLeft);
	/**
	@brief TODO
	*/
	void update(int time);

	float destinationScale();
	//float destinationHeight(float h = 2);

protected:
	SDL_Texture * spriteSheet;
	SDL_Rect sourceRect;

	Location loc;
private:
};

#endif