#include "Tile.h"



Tile::Tile(){

}

Tile::Tile(Sprite _sprite) {
	sprite = _sprite;
}

/*
Default graphics
*/
Tile::Tile(Graphics &graphics) {
	sprite = Sprite(
		graphics,
		"C:\\Users\\jmhug\\source\\repos\\Engine\\ground.png",
		Location(0, 0),
		160, 160,
		Location(0, 0)
	);
}

Tile::~Tile(){

}

void Tile::draw(Graphics &graphics, SDL_Rect camera, Location loc) {
	sprite.draw(graphics, Location(loc.getX() - camera.x, loc.getY() - camera.y), true);
}