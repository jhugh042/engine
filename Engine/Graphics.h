#pragma once

#include <string>
#include <map>
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "Globals.h"

struct SDL_Window;
struct SDL_Renderer;

class Graphics
{
public:
	Graphics();
	~Graphics();

	/**
	@brief initializes the window
	*/
	void initialize();

	/**
	@brief loads the sprite sheets for future use
	@param file location
	@return SDL_Surface*
	*/
	SDL_Surface* loadImage(const std::string &filePath);

	/**
	@brief rendeds the image to screen
	@param	source of sprite, source of rectangle in sprite,
			where to draw the rectangle, flip the image?
	*/
	void blitSurface(SDL_Texture *source, SDL_Rect* sourceRectangle, SDL_Rect* destRectangle, bool facingLeft);

	/**
	@brief test code that draws a color box
	*/
	void drawColoredBox(SDL_Rect* rect);

	/**
	@brief presents the image
	*/
	void flip();

	/**
	@brief clears the graphics drawn
	*/
	void clear();
	SDL_Renderer* getRenderer() const;

private:
	SDL_Window *window;
	SDL_Renderer *renderer;

	std::map<std::string, SDL_Surface*> spriteSheets;
};

