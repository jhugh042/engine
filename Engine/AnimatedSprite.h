#ifndef ANIMATEDSPRITE_H
#define ANIMATEDSPRITE_H

#include <vector>
#include "Sprite.h"

class AnimatedSprite : public Sprite
{
public:
	AnimatedSprite();

	AnimatedSprite(Graphics &graphics, const std::string &filePath,
		Location sourceLoc, int width, int height,
		Location posLoc, int _aFrames[],
		Location aStartLoc[], std::pair<SDL_Scancode, SDL_Scancode> _aName[], int _aWidth[],
		int _aHeight[], Location _offset[], int _numanimations);
	
	~AnimatedSprite();

	/**
	@brief draws the sprite to the screen
	@param Graphics, Location, Bool to flip
	*/
	void draw(Graphics &graphics, Location _loc, bool facingLeft);

	/**
	@brief uses the elapsed time to decide to change to next animation frame
	@param int elapsedTime
	*/
	void update(int elapsedTime);


	/**
	@brief Plays the animation provided if it's not already playing
	@param button linked to animation, repeat bool
	*/
	void playAnimation(std::pair<SDL_Scancode, SDL_Scancode> animation, bool once);

	/**
	@brief Stops the current animation
	*/
	void stopAnimation();

protected:

	/**
	@brief adds animation to the map of possible ones
	@param frames in animation, start location, button linked to animation, width, height, offset between frames
	*/
	void addAnimation(int _frames, Location startLoc, std::pair<SDL_Scancode, SDL_Scancode> name, int width, int height, Location offset);
	/*
	@brief Changes the visibility of the animated sprite
	@param bool visible
	*/
	void setVisible(bool visible);

	double timeToUpdate = 60;
	bool currentAnimationOnce;
	std::pair<SDL_Scancode, SDL_Scancode> currentAnimation;
private:
	std::map<std::pair<SDL_Scancode, SDL_Scancode>, std::vector<SDL_Rect> > animations;

	int frameIndex;
	double timeElapsed = 0;
	bool visible = false;
};

#endif
