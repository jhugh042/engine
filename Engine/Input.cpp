#include "Input.h"

void Input::update() {
	pressedKeys.clear();
	releasedKeys.clear();
}

void Input::keyUp(const SDL_Event &event) {
	releasedKeys[event.key.keysym.scancode] = true;
	heldKeys[event.key.keysym.scancode] = false;

}
void Input::keyDown(const SDL_Event &event) {
	pressedKeys[event.key.keysym.scancode] = true;
	heldKeys[event.key.keysym.scancode] = true;
}

bool Input::keyPressed(SDL_Scancode key) {
	return pressedKeys[key];
}
bool Input::keyReleased(SDL_Scancode key) {
	return releasedKeys[key];
}
bool Input::keyHeld(SDL_Scancode key) {
	return  heldKeys[key];
}

void Input::clearAllKeys() {
	heldKeys.clear();
	pressedKeys.clear();
	releasedKeys.clear();
}

bool Input::keyHandler(Entity &player) {
	if (keyPressed(ESCAPE) == true) {
		return false;
	}
	//movement
	//NW
	if (keyHeld(MOVE_LEFT) && keyHeld(MOVE_UP)) {
		player.moveEntity(-1, -1, MOVE_UP, MOVE_LEFT);
	}//SW
	else if (keyHeld(MOVE_LEFT) && keyHeld(MOVE_DOWN)) {
		player.moveEntity(-1, 1, MOVE_DOWN, MOVE_LEFT);
	}//NE
	else if (keyHeld(MOVE_RIGHT) && keyHeld(MOVE_UP)) {
		player.moveEntity(1, -1, MOVE_UP, MOVE_RIGHT);
	}//SE
	else if (keyHeld(MOVE_RIGHT) && keyHeld(MOVE_DOWN)) {
		player.moveEntity(1, 1, MOVE_DOWN, MOVE_RIGHT);
	}//WEST
	else if (keyHeld(MOVE_LEFT)) {
		player.moveEntity(-1, 0, MOVE_LEFT, UNKNOWN);
	}//EAST
	else if (keyHeld(MOVE_RIGHT)) {
		player.moveEntity(1, 0, MOVE_RIGHT, UNKNOWN);
	}//NORTH
	else if (keyHeld(MOVE_UP)) {
		player.moveEntity(0, -1, MOVE_UP, UNKNOWN);
	}//SOUTH
	else if (keyHeld(MOVE_DOWN)) {
		player.moveEntity(0, 1, MOVE_DOWN, UNKNOWN);
	}
	if (keyReleased(MOVE_LEFT) || keyReleased(MOVE_RIGHT)
		|| keyReleased(MOVE_UP) || keyReleased(MOVE_DOWN)) {
		player.stopAnimation();
	}

	if (keyPressed(HUD_1)) {
		player.changeHUDVisiblity(HUD_1);
	}
	return true;
}