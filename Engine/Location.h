#pragma once

class Location {
public:
	Location() {
		x = 0;
		y = 0;
	}
	Location(float _x, float _y) {
		x = _x;
		y = _y;
	}
	~Location() {}

	float getX() {
		return x;
	}
	float getY() {
		return y;
	}

	void setX(float _x) {
		x = _x;
	}

	void setY(float _y) {
		y = _y;
	}
private:
	float x;
	float y;
};