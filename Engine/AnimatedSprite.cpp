#include "AnimatedSprite.h"



AnimatedSprite::AnimatedSprite(){

}

AnimatedSprite::AnimatedSprite(Graphics &graphics, const std::string &filePath,
	Location sourceLoc, int width, int height,
	Location posLoc, int _aFrames[],
	Location aStartLoc[], std::pair<SDL_Scancode, SDL_Scancode> _aName[], int _aWidth[],
	int _aHeight[], Location _offset[], int _numanimations) :
	Sprite(graphics, filePath, sourceLoc,width,height,posLoc){

	//animations
	for (int i = 0; i < _numanimations; i++) {
		addAnimation(_aFrames[i], aStartLoc[i], _aName[i],
			_aWidth[i], _aHeight[i], _offset[i]);

	}
	currentAnimation = _aName[0];
}


AnimatedSprite::~AnimatedSprite(){
	animations.clear();
}

void AnimatedSprite::draw(Graphics &graphics, Location _loc, bool facingLeft) {

	//destination to draw sets width and height using source rectangle
	SDL_Rect destinationRect = { _loc.getX(), _loc.getY(), 
					sourceRect.w * destinationScale(), sourceRect.h * destinationScale() };
	if (visible) {
		//std::cout << "\nAnnimation: "<<currentAnimation<<"Frameindex: " << frameIndex;
		SDL_Rect _sourceRect = animations[currentAnimation][frameIndex];
		graphics.blitSurface(spriteSheet, &_sourceRect, &destinationRect, facingLeft);
	}
	else {
		SDL_Rect _sourceRect = animations[currentAnimation][frameIndex];
		graphics.blitSurface(spriteSheet, &_sourceRect, &destinationRect, facingLeft);
	}
}

/*
updates animation frames
*/
void AnimatedSprite::update(int elapsedTime) {

	timeElapsed += elapsedTime;
	if (timeElapsed > timeToUpdate) {
		timeElapsed -= timeToUpdate;
		if (frameIndex < animations[currentAnimation].size() - 1) {
			if (visible) {
				frameIndex++;
			}
		}
		else {
			if (currentAnimationOnce == true) {
				setVisible(false);
				stopAnimation();
			}
			else {
				frameIndex = 0;
			}
		}
	}
}

/*
Adds frams to animation based on starting x and y locations and how many frames there are
*/
void AnimatedSprite::addAnimation(int frames, Location startLoc, std::pair<SDL_Scancode, SDL_Scancode> name, int width, int height, Location offset) {
	std::vector<SDL_Rect> rectangles;
	for (int i = 0; i < frames; i++) {

		SDL_Rect newRect = { (i + startLoc.getX()) + (i * width) + (i * offset.getX()), 
			startLoc.getY(), width, height };

		rectangles.push_back(newRect);
	}
	animations.insert(std::make_pair(name, rectangles));
}

/*

*/
void AnimatedSprite::playAnimation(std::pair<SDL_Scancode, SDL_Scancode> animation, bool once) {
	setVisible(true);
	currentAnimationOnce = once;
	if (currentAnimation != animation) {
		currentAnimation = animation;
		frameIndex = 0;
	}
}

/* void stopAnimation
* Stops the current animation
*/
void AnimatedSprite::stopAnimation() {
	frameIndex = 0;
	//currentAnimation = std::make_pair(SDL_SCANCODE_UNKNOWN, SDL_SCANCODE_UNKNOWN);
	setVisible(false);
}

/* void setVisible
* Changes the visibility of the animated sprite
*/

void AnimatedSprite::setVisible(bool _visible) {
	visible = _visible;
}