#include "Game.h"




Game::Game(){
	printf("Initializing SDL2\n");
	if (SDL_Init(SDL_INIT_EVERYTHING)) {
		std::cout << "Error: " << SDL_GetError() << "\n";
	}
	printf("Initializing SDL_Mixer...\n");
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		printf("Failed to initialize SDL_Mixer\n");
	}
	Mix_Volume(-1, globals::SOUND_EFFECT_VOLUME * 100);
	Mix_VolumeMusic(globals::BACKGROUND_VOLUME * 100);
}

Game::~Game() {

}

void Game::start() {


	SDL_Thread *thread;
	if (!map.setBGMusic("../Resources/background.mp3")) {
		printf("Failed loading Audio\n");
	}

	if (!map.playBGMusic()) {
		printf("Failed to start playing background music\n");
	}

	if (!player.addSoundEffect("BELL", "../Resources/soundEffect.wav")) {
		printf("Failed loading soundEffect\n");
	}
	
	int LAST_UPDATE_TIME = SDL_GetTicks();
	float probabilityIncrease = 0;
	while (true) {

		input.update();

		if (SDL_PollEvent(&event)) {
			if (event.type == SDL_KEYDOWN) {
				if (event.key.repeat == 0) {
					input.keyDown(event);
				}
			}
			else if (event.type == SDL_KEYUP) {
				input.keyUp(event);
			}
			if (event.type == SDL_QUIT) {
				return;
			}
			if (event.key.keysym.sym == SDLK_4) {
				player.playSoundEffect("BELL");
			}
		}
		if (!input.keyHandler(player)) {
			return;
		}
		const int CURRENT_TIME_MS = SDL_GetTicks();
		int ELAPSED_TIME_MS = CURRENT_TIME_MS - LAST_UPDATE_TIME;
		LAST_UPDATE_TIME = CURRENT_TIME_MS;


		update(ELAPSED_TIME_MS);
		draw();
		if (randomEnemyEncounter(probabilityIncrease)) {
			probabilityIncrease = 0;
			LAST_UPDATE_TIME = SDL_GetTicks();
			input.clearAllKeys();
			player.stopAnimation();
		}
		else {
			probabilityIncrease += 0.1;
		}
	}
	SDL_Quit();
}

void Game::draw() {
	graphics.clear();
	if (!map.draw(graphics, camera)) {
		player.revertMove();
	}
	player.draw(graphics, camera);
	graphics.flip();
}


void Game::update(int _ELAPSED_TIME) {
	player.getSprite().update(_ELAPSED_TIME);
	previous = Location(camera.x, camera.y);
	camera.x = player.getLocation().getX() - (globals::SCREEN_WIDTH / 2);
	camera.y = player.getLocation().getY() - (globals::SCREEN_HEIGHT / 2);
}

void Game::setGraphics(Graphics &_graphics) {
	graphics = _graphics;
}

void Game::setPlayer(Entity &_player) {
	player = _player;
}

void Game::setMap(Map &_map) {
	map = _map;
}
bool Game::randomEnemyEncounter(float value) {
	if (previous.getX() != camera.x || previous.getY() != camera.y) {
		srand(time(NULL));
		int number = 5;
		float base = 0 + value;
		bool random = ((rand() % 100 + 1) < base);
		if (random) {
			std::cout << "\nEncounter\n";
			//start battle start animation music
			Battle battle = Battle(battleMusic,battleStart);
			battle.battleStartAnimation(graphics, map, player, camera);

			if (!battle.startBattleLoop()) {
				//player died
			}

			battle.pauseBGMusic();

			map.playBGMusic();
			return true;
		}
	}
	return false;
}